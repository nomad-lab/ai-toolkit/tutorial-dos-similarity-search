from nomad.datamodel.datamodel import EntryArchive
from nomad_dos_fingerprints import DOSFingerprint

def calc_id(db_entry: EntryArchive) -> str:
    '''
    Retrieve the calculation id from an Archive entry
    '''
    try:
        return db_entry.metadata.calc_id
    except KeyError:
        return db_entry.metadata.entry_id

def upload_id(db_entry: EntryArchive) -> str:
    '''
    Retrieve the upload id from an Archive entry
    '''
    return db_entry.metadata.upload_id

def url_endpoint(db_entry: EntryArchive) -> str:
    '''
    Retrieve the url endpoint to access the NOMAD Archive webpage
    '''
    url1 = upload_id(db_entry)
    url2 = calc_id(db_entry)
    return url1 + '/' + url2

def formula(db_entry: EntryArchive) -> str:
    '''
    Retrieve the chemical formula.
    '''
    return db_entry.results.material.chemical_formula_reduced

def material_id(db_entry: EntryArchive) -> str:
    '''
    Retrieve the material id of the Archive entry
    '''
    return db_entry.results.material.material_id

def space_group_number(db_entry: EntryArchive) -> str:
    '''
    Retrieve the space group number of the Archive entry
    '''
    return db_entry.results.material.symmetry.space_group_number

def dos_fingerprint(db_entry: EntryArchive) -> DOSFingerprint:
    '''
    Generate a DOS fingerprint object from an Archive entry
    '''
    dos_fp = db_entry.run[0].calculation[-1].dos_electronic[0].fingerprint
    return DOSFingerprint.from_dict(dos_fp)


def DOS(db_entry: EntryArchive) -> dict:
    '''
    Retrieve the density of states spectrum
    '''
    normalization_factor = db_entry.run[0].calculation[-1].dos_electronic[0].total[0].normalization_factor
    offset = db_entry.run[0].calculation[-1].energy.highest_occupied.to("eV")
    energies = db_entry.run[0].calculation[-1].dos_electronic[0].energies.to('eV') - offset
    values = db_entry.run[0].calculation[-1].dos_electronic[0].total[0].value.to('1/eV')*normalization_factor.magnitude
    spectrum = {'energies': energies, 'values': values}
    return spectrum

