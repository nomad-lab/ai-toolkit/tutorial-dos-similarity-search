{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<img  src=\"assets/dos_similarity_search/header.jpg\" width=\"900\"> "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "<img style=\"float: left;\" src=\"assets/dos_similarity_search/logo_MPG.png\" width=150>\n",
    "<img style=\"float: left; margin-top: -10px\" src=\"assets/dos_similarity_search/logo_NOMAD.png\" width=250>\n",
    "<img style=\"float: left; margin-top: -5px\" src=\"assets/dos_similarity_search/logo_HU.png\" width=130>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<span style='font-family:sans-serif'>\n",
    "\n",
    "## Introduction\n",
    "\n",
    "This notebook shows how to compute the similarity of materials in terms of their electronic density-of-states (DOS) from data retrieved from the [NOMAD Archive](https://nomad-lab.eu/prod/v1/gui/search/entries). \n",
    "\n",
    "For this purpose, a _DOS fingerprint_ is used which encodes the DOS obtained from density-functional theory (DFT) calculations into a binary valued descriptor. A detailed description of the fingerprint can be found in Ref. [1].\n",
    "\n",
    "The DOS fingerprints in this notebook are precomputed and available in the NOMAD Archive. \n",
    "We first download the respective data from the NOMAD Archive and use the fingerprint to find materials that are similar to a given reference material.\n",
    "\n",
    "**In this notebook we demonstrate how to find GaAs-based binary and ternary compounds from the NOMAD Archive that have the most similar electronic structure to GaAs.**\n",
    "\n",
    "### Contents:\n",
    "- [Import modules](#Import-modules)\n",
    "- [Downloading data from the NOMAD Archive](#Downloading-data-from-the-NOMAD-Archive)\n",
    "  - [Downloading a single calculation](#Downloading-a-reference-material)\n",
    "  - [Downloading calculations using search queries](#Downloading-calculations-using-search-queries)\n",
    "- [The DOS fingerprint as a descriptor](#The-DOS-fingerprint-as-a-descriptor)\n",
    "- [Calculation of similarity coefficients](#Calculation-of-similarity-coefficients)\n",
    "- [Visualizing results](#Visualizing-results)\n",
    "- [References](#References)\n",
    "\n",
    "</span>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<span style='font-family:sans-serif'>\n",
    "\n",
    "## Import modules\n",
    "\n",
    "To interact with the NOMAD Archive API we use the python package `nomad-lab`. To learn more about its usage, please refer to the [documentation](https://nomad-lab.eu/prod/rae/docs/client/client.html). </span>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import pandas as pd\n",
    "import matplotlib.pyplot as plt\n",
    "import nest_asyncio\n",
    "import copy\n",
    "\n",
    "from nomad.client import ArchiveQuery\n",
    "\n",
    "# Load plot parameters\n",
    "plt.style.use('./data/dos_similarity_search/dos_similarity.mplstyle')\n",
    "\n",
    "nest_asyncio.apply()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<span style='font-family:sans-serif'>\n",
    "\n",
    "# Downloading data from the NOMAD Archive\n",
    "\n",
    "**For a detailed overview on how to query the NOMAD Archive using the `nomad-lab` package see the tutorial 'Query the Archive' on the ['AI toolkit tutorials'](https://nomad-lab.eu/services/aitoolkit) page.** Here, we will download all necessary data to perform a similarity search using DOS fingerprints. This is achieved using an instance of `ArchiveQuery`. It allows for querying the NOMAD Achive with only few commands. \n",
    "</span>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<span style='font-family:sans-serif'>\n",
    "\n",
    "## Downloading a reference material\n",
    "\n",
    "First, we download a reference calculation for [GaAs](https://nomad-lab.eu/prod/v1/gui/entry/id/SobHx8fSRuC0eC3fZuRPLA/Ya3jm8nB0Gb_8VFZs2j1dNYAg7h_) from the Archive. To download a specific calculation we construct the `query` dictionary only from the calculation ID. The calculation ID is a unique, static identifier for each calculation. \n",
    "\n",
    "For the here presented analysis, not all of the data of a calculation are required. Therefore, we select the paths to the needed data in the NOMAD Archive entry. The paths are contained in the cell below in the variable `reference_query_required_sections`. This helps to reduce unnecessary download of data. The path to all data of a calculation can be found on the the NOMAD [Metainfo](https://nomad-lab.eu/prod/v1/gui/analyze/metainfo) page.</span> "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "reference_calc_id = 'zkkMIAPyn4OCbdEdW21DZTeretQ3'\n",
    "reference_query_parameters = {\n",
    "    'entry_id': reference_calc_id # ID of the reference calculation\n",
    "}\n",
    "reference_query_required_sections = {\n",
    "    # DOS fingerprint\n",
    "    'workflow': { \n",
    "        'calculation_result_ref': {\n",
    "            'dos_electronic': {\n",
    "                'fingerprint': '*'\n",
    "            }\n",
    "        }\n",
    "    },\n",
    "    # Upload and calculation id\n",
    "    'metadata': \"*\",\n",
    "    # chemical formula, material id, and space group number\n",
    "    'results':{\n",
    "        'material':{\n",
    "            'chemical_formula_reduced': '*',\n",
    "            'material_id': '*',\n",
    "            'symmetry': {\n",
    "                'space_group_number': '*'\n",
    "            }\n",
    "        }\n",
    "    }\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# compile the query\n",
    "reference_GaAs_query = ArchiveQuery(query = reference_query_parameters, \n",
    "                                    required = reference_query_required_sections)\n",
    "# download\n",
    "reference_GaAs = reference_GaAs_query.download()[0]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<span style='font-family:sans-serif'>\n",
    "\n",
    "This calculation stored in the variable `reference_GaAs` will be used as a reference for our similarity search. \n",
    "</span>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<span style='font-family:sans-serif'>\n",
    "\n",
    "## Downloading calculations using search queries\n",
    "\n",
    "To perfrom a similarity search, we compare the fingerprint of the reference to the fingerprints of a large data set.\n",
    "In the following, we query the NOMAD Archive for GaAs-based binary and ternary compounds. As a starting point we restrict the search to only calculations computed with the DFT code 'VASP' using a GGA exchange-correlation functional. This information is written to the `query` dictionary that is passed to `ArchiveQuery`. Note that the `required` argument of the `ArchiveQuery` is unchanged.\n",
    "</span>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<span style='font-family:sans-serif'>\n",
    "\n",
    "**Queries can be generated in the GUI of the [NOMAD Archive](https://nomad-lab.eu/prod/v1/gui/search) in the python dictionary format.** They can be found under the `<>` symbol at the top of the search menu. From there, they can be directly copied into the `query` dictionary of the `ArchiveQuery` function.\n",
    " </span>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "search_query_parameters = {\n",
    "    'results.method.simulation.program_name': 'VASP',\n",
    "    'results.material.elements': {'all': ['Ga', 'As']},\n",
    "    'results.properties.available_properties': ['dos_electronic'],\n",
    "    'results.method.simulation.dft.xc_functional_type': ['GGA'],\n",
    "    'results.material.n_elements': {'gte': 2, 'lte': 3}\n",
    "}\n",
    "\n",
    "# the required parameters are the same as for the reference\n",
    "GaAs_alloy_query = ArchiveQuery(query =  search_query_parameters,\n",
    "                                 required = reference_query_required_sections,\n",
    "                                 page_size=1000, results_max=10000)\n",
    "\n",
    "\n",
    "GaAs_alloy = GaAs_alloy_query.download()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<span style='font-family:sans-serif'>\n",
    "\n",
    "Next, we remove entries without DOS fingerprint data.\n",
    "</span>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [],
   "source": [
    "# removing entries with empty fingerprints\n",
    "index = 0\n",
    "while index < len(GaAs_alloy):\n",
    "    try:\n",
    "        assert GaAs_alloy[index].run[0].calculation[-1].dos_electronic[0].fingerprint.bins != ''\n",
    "        index += 1\n",
    "    except (IndexError, AssertionError):\n",
    "        del GaAs_alloy[index]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<span style='font-family:sans-serif'>\n",
    "\n",
    "# The DOS fingerprint as a descriptor\n",
    "\n",
    "In order to quantitatively evaluate materials similarity, we encode the electronic DOS in a so-called _DOS fingerprint_. The DOS fingerprint is a two-dimensional, binary-valued representation of the electronic DOS. An in-depth description can be found in Ref. [1].\n",
    "\n",
    "To make use of the fingerprint, the data stored in the NOMAD Archive must be loaded into `DOSFingerprint` objects. Therefore, we scan through the Archive contents that we downloaded previously and extract all data that are related to the fingerprint, as well as identifiers for presenting the results. To do so in a systematic manner, we define functions that collect the relevant information from an Archive entry. An example of such a function, `formula`, is given below. These function are passed in a list `exctract_properties` to the function `get_data`, which extracts the relevant data from `ArchiveQuery`. The extracted data is saved using the name of the function as the keyword.\n",
    "\n",
    "For convenience, the extracted data are collected in a dictionary which will allow us to efficienty search the results.\n",
    "</span>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [],
   "source": [
    "from nomad.datamodel.datamodel import EntryArchive\n",
    "from dos_similarity_search.extract_data import *\n",
    "from dos_similarity_search.tools import *\n",
    "\n",
    "def formula(db_entry: EntryArchive) -> str:\n",
    "    '''\n",
    "    Retrieve the chemical formula.\n",
    "    '''\n",
    "    return db_entry.results.material.chemical_formula_reduced\n",
    "\n",
    "# Extract data and apply filters\n",
    "extract_properties = [calc_id, upload_id, url_endpoint, formula, material_id, space_group_number, dos_fingerprint]\n",
    "\n",
    "materials_data = {}\n",
    "for calculation in GaAs_alloy:\n",
    "    materials_data[calc_id(calculation)] = get_fingerprint_data(calculation, extract_properties)\n",
    "\n",
    "reference = {calc_id(reference_GaAs): get_fingerprint_data(reference_GaAs, extract_properties)}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<span style='font-family:sans-serif'>\n",
    "\n",
    "The Archive API returns all calculations which fit the query, therefore, **for a single material multiple calculations (e.g. from different authors) are downloaded.** \n",
    "\n",
    "To simplify the analysis presented here, **we select a representative calculation for each material**. To do so, we define a function called `select_representative`. The cell below shows an example of this function that takes the first encountered calculation of a material as the representative. However, different approaches can be used, e.g., based on computational parameters employed in the DFT calculations.\n",
    "</span>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [],
   "source": [
    "def select_representative(materials_data: list) -> list:\n",
    "    '''\n",
    "    Example of a `select_representative` function.\n",
    "    Returns the first calculation of a material it finds.\n",
    "    \n",
    "    Inputs:\n",
    "        materials_data: list, containing the materials in a dictionary as outputted by `get_fps`\n",
    "    '''\n",
    "    material_ids = []\n",
    "    output = copy.deepcopy(materials_data)\n",
    "\n",
    "    for calc_id, properties in materials_data.items():\n",
    "        material_id = properties['material_id']\n",
    "        if material_id in material_ids:\n",
    "            del output[calc_id]\n",
    "        else:\n",
    "            material_ids.append(material_id)\n",
    "\n",
    "    return output"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<span style='font-family:sans-serif'>\n",
    "\n",
    "# Calculation of similarity coefficients\n",
    "\n",
    "Now we compute the similarity between two DOS spectra.\n",
    "\n",
    "A DOS fingerprint represents the electronic DOS as a binary vector [1]. In order to compute the similarity of two fingerprints we use the **Tanimoto coefficient** [2]. The Tanimoto coefficient, $T_c$, between two vectors $\\mathbf{a}$ and $\\mathbf{b}$ is defined as:\n",
    "\n",
    "\n",
    "$$\\begin{eqnarray}\n",
    "T_c(\\mathbf{a},\\mathbf{b}) = \\frac{\\mathbf{a} \\cdot \\mathbf{b}}{||\\mathbf{a}||^2 + ||\\mathbf{b}||^2 - \\mathbf{a} \\cdot \\mathbf{b}}.\n",
    "\\end{eqnarray}$$\n",
    "\n",
    "\n",
    "It is restricted to values $T_c \\in [0,1]$. 1 means that the DOS of two materials are identical, 0 means no overlap at all. **The Tanimoto coefficient can be interpreted as the ratio between the number of shared features and the total number of features of two fingerprints.** For dichotomous vectors, the complement of the Tanimoto coefficient ($1 - T_c$), also known as Jaccard distance, is a metric. The Tanimoto coefficient is implemented as the function `tanimoto_similarity` in the `nomad_dos_fingerprints` package.\n",
    "\n",
    "The arguments of the function `tanimoto_similarity` are two `DOSFingerprint` objects. Using this, the similarity between the reference material and one of the candidate materials can be calculated, as shown in the following example:\n",
    "</span>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from nomad_dos_fingerprints import tanimoto_similarity\n",
    "\n",
    "reference_values = list(reference.values())[0]\n",
    "candidate_values = list(materials_data.values())[0]\n",
    "\n",
    "print(f\"Similarity between {reference_values['formula']} and {candidate_values['formula']}:\\n\")\n",
    "print(f\"Tc = {tanimoto_similarity(reference_values['dos_fingerprint'], candidate_values['dos_fingerprint'])}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<span style='font-family:sans-serif'>\n",
    "\n",
    "Now we **use the function `calculate_similarity` to calculate the similarities of the materials in `materials_data` to our reference**, `reference`. The function `calculate_similarity` returns a dictionary of the calculation, where `similarity to <reference_formula>` is the value of the Tanimoto coefficient between the reference and the current calculation. \n",
    "</span>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [],
   "source": [
    "materials_data = select_representative(materials_data)\n",
    "# apply `calculate_similarity` to all entries in `materials_data`\n",
    "for key, properties in materials_data.items(): calculate_similarity(properties, reference)\n",
    "# sort `materials_data` from highest similarity to lowest\n",
    "materials_data = dict(sorted(materials_data.items(), key = lambda x: x[1][f'similarity to {reference_values[\"formula\"]}'], reverse = True))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<span style='font-family:sans-serif'>\n",
    "\n",
    "Now we have computed the similarities of the `reference` to all the materials in `materials_data`.\n",
    "</span>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<span style='font-family:sans-serif'>\n",
    "\n",
    "# Visualizing results\n",
    "\n",
    "We want to look at the results of the similarity search to identify the most similar materials to the reference. For an overview, we first visualize the found similarity coefficients in a histogram.\n",
    "</span>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "reference_formula = str(reference_values['formula'])\n",
    "\n",
    "plt.figure(figsize = (13,5))\n",
    "formulas = [value['formula'] for key, value in materials_data.items()]\n",
    "similarities = [value['similarity to ' +str(reference_formula)] for key,value in materials_data.items()]\n",
    "plt.hist(similarities, bins = 20, range = [0,1], label = f'Reference: {reference_formula}')\n",
    "plt.xticks(np.arange(0, 1.1, 0.1))\n",
    "plt.xlabel('Tc')\n",
    "plt.xlim(0,1)\n",
    "plt.ylabel('Counts')\n",
    "plt.yscale('log')\n",
    "#plt.title('Frequency of similarity coefficients')\n",
    "plt.legend(fontsize = 20)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<span style='font-family:sans-serif'>\n",
    "\n",
    "Note the logarithmic scale in this histogram. We can see here, that the vast majority of materials has a low similarity to our reference. On the right side of the histogram ($\\mathrm{Tc} > 0.7$), the most similar materials can be found, which show exceptionally high similarity scores.\n",
    "\n",
    "We construct a ranking table which shows the similarity of materials to our reference from the most similar to the least similar.\n",
    "</span>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for key, value in materials_data.items():\n",
    "    value[\"formula (link)\"] = (value[\"formula\"], value[\"url_endpoint\"])\n",
    "\n",
    "def make_clickable(x):\n",
    "    return f'<a target=\"{x[1]}\" href=\"https://nomad-lab.eu/prod/v1/gui/entry/id/{x[1]}\">{x[0]}</a>'\n",
    "\n",
    "ranking_table = pd.DataFrame([value for key, value in materials_data.items()])\n",
    "ranking_table[['formula (link)', 'space_group_number', f'similarity to {reference_formula}']].style.format({'formula (link)' : make_clickable})"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<span style='font-family:sans-serif'>\n",
    "\n",
    "By clicking the link, you will land on the Archive page of the respective calculation, where you can find more information about your material.\n",
    "\n",
    "Now we plot the DOS of the most similar materials to our reference. In the variable `ranks_to_download`, we give the rank of the materials from the table above, whose DOS we want to plot. To avoid unnecessary downloading, we check if the spectrum is already in `materials_data` under the key `dos`, if not, we download it.\n",
    "</span>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from dos_similarity_search.tools import download_DOS, DOS_downloaded\n",
    "\n",
    "ranks_to_download = list(range(3))\n",
    "\n",
    "#get calc_id from table\n",
    "calc_ids = []\n",
    "for rank in ranks_to_download:\n",
    "    calc_ids.append(ranking_table.iloc[rank]['calc_id'])\n",
    "    \n",
    "# check if already downloaded\n",
    "calc_ids_to_download = [calculation_id for calculation_id in calc_ids if not DOS_downloaded(materials_data, calculation_id)]\n",
    "\n",
    "#download DOS spectrum\n",
    "dos_spectra = download_DOS(calc_ids_to_download)\n",
    "for calculation_id, dos_spectrum in dos_spectra.items():\n",
    "    materials_data[calculation_id]['dos'] = dos_spectrum\n",
    "\n",
    "if not DOS_downloaded(reference, reference_calc_id):\n",
    "    reference_dos_spectrum = download_DOS(reference_calc_id)\n",
    "    for calculation_id, dos_spectrum in reference_dos_spectrum.items():\n",
    "        reference[calculation_id]['dos'] = dos_spectrum\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<span style='font-family:sans-serif'>\n",
    "And finally we plot the spectra.\n",
    "</span>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.figure(figsize = (13,5))\n",
    "dos_energies_reference = reference[reference_calc_id]['dos']['energies']\n",
    "dos_values_reference = reference[reference_calc_id]['dos']['values']\n",
    "chem_formula_reference = reference[reference_calc_id]['formula']\n",
    "\n",
    "plt.plot(dos_energies_reference.magnitude, dos_values_reference.magnitude, label = 'reference', c = 'r')\n",
    "plt.fill_between(dos_energies_reference.magnitude, dos_values_reference.magnitude, color = 'r', alpha = 0.1)\n",
    "\n",
    "for calculation_id in calc_ids:\n",
    "    dos_energies = materials_data[calculation_id]['dos']['energies']\n",
    "    dos_values = materials_data[calculation_id]['dos']['values']\n",
    "    chem_formula = materials_data[calculation_id]['formula']\n",
    "    Tc = materials_data[calculation_id][f'similarity to {reference_formula}']\n",
    "\n",
    "    plt.plot(dos_energies.magnitude, dos_values.magnitude, label = f\"{chem_formula}, Tc = {Tc: .2f}\")\n",
    "    plt.fill_between(dos_energies.magnitude, dos_values.magnitude, alpha = 0.1)\n",
    "    plt.ylabel(r'DOS [$\\frac{1}{eV}$]')\n",
    "    plt.xlabel(r'Energy [$eV$]')\n",
    "\n",
    "plt.xlim(-10, 5)\n",
    "plt.ylim(0,2)\n",
    "plt.legend(prop={'size': 15})\n",
    "plt.show()\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<span style='font-family:sans-serif'>\n",
    "\n",
    "# References\n",
    "\n",
    "[1] M. Kuban, S. Rigamonti, M. Scheidgen, and C. Draxl: [Density-of-states similarity descriptor for unsupervised learning from materials data](https://arxiv.org/abs/2201.02187)\n",
    "\n",
    "[2] P. Willet, J. M. Barnard, G. M. Downs: [Chemical Similarity Searching](https://pubs.acs.org/doi/abs/10.1021/ci9800211), $\\textit{J. Chem. Inf. Comput. Sci.}$, $\\textbf{38}$, 983, (1998)\n",
    "</span>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<span style='font-family:sans-serif'>\n",
    "\n",
    "# Acknowledgements\n",
    "\n",
    "We thank Luca Ghiringhelli for help in preparing this notebook. This work recieved partial funding from the European Union’s Horizon 2020 research and innovation program under the grant agreement Nº 951786 (NOMAD CoE), from the NFDI consortium FAIRmat, and from the German Research Foundation (DFG) through the CRC 1404 (FONDA).\n",
    "</span>"
   ]
  }
 ],
 "metadata": {
  "hide_input": false,
  "interpreter": {
   "hash": "31f2aee4e71d21fbe5cf8b01ff0e069b9275f58929596ceb00d14d90e3e16cd6"
  },
  "kernelspec": {
   "display_name": "analytics_dos_similarity_search_venv",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.4"
  },
  "metadata": {
   "interpreter": {
    "hash": "2dfbd2783628fbe9267bf55a6c8862bcee5db4b092f4a0aebb9a9eab56757fa6"
   }
  },
  "toc": {
   "base_numbering": "0",
   "nav_menu": {},
   "number_sections": false,
   "sideBar": true,
   "skip_h1_title": true,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {},
   "toc_section_display": true,
   "toc_window_display": true
  },
  "varInspector": {
   "cols": {
    "lenName": 16,
    "lenType": 16,
    "lenVar": 40
   },
   "kernels_config": {
    "python": {
     "delete_cmd_postfix": "",
     "delete_cmd_prefix": "del ",
     "library": "var_list.py",
     "varRefreshCmd": "print(var_dic_list())"
    },
    "r": {
     "delete_cmd_postfix": ") ",
     "delete_cmd_prefix": "rm(",
     "library": "var_list.r",
     "varRefreshCmd": "cat(var_dic_list()) "
    }
   },
   "types_to_exclude": [
    "module",
    "function",
    "builtin_function_or_method",
    "instance",
    "_Feature"
   ],
   "window_display": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
